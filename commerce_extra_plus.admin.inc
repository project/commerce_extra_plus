<?php


function commerce_extra_plus_admin_form() {
  
  $form['options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Configuration Options'),
  );
  
  $form['options']['commerce_extra_plus_email_auto_username'] = array(
    '#type'  => 'checkbox',
    '#title' => t('Hide username field on registration form and create it automatically.'),    
    '#default_value' => variable_get('commerce_extra_plus_email_auto_username', 1),    
    '#description' => t('Enable this option to hide the username field on the checkout registration form and generate it automatically.'),    
  );
  $roles = user_roles();
  $form['options']['commerce_extra_plus_by_pass_roles'] = array(
    '#type'  => 'checkboxes',
    '#options' => $roles,
    '#title' => t('Select any roles that will bypass the auto username generation.'),    
    '#default_value' => variable_get('commerce_extra_plus_by_pass_roles', array()),    
    '#description' => t('Any roles selected above will not have the automatic usernames generated.'),
    '#states' => array(
      'invisible' => array(
          ':input[name="commerce_extra_plus_email_auto_username"]' => array('checked' => FALSE),
        ),
      ),      
   );
  $form['options']['commerce_extra_plus_email_login'] = array(
    '#type'  => 'checkbox',
    '#title' => t('Allow logging with email address as well as username during checkout.'),    
    '#default_value' => variable_get('commerce_extra_plus_email_login', 1),    
    '#description' => t('Enable this option to allow the user to login to their account using their email address as well as their username.'),    
  );
  
  $form['options']['commerce_extra_plus_disable_reg'] = array(
    '#type'  => 'checkbox',
    '#title' => t('Disable Registration Page'),    
    '#default_value' => variable_get('commerce_extra_plus_disable_reg', 1),    
    '#description' => t('When enabled the registration page (user/registration) will be disabled to prevent creating new user accounts without checking out.'),    
  );

  $form['#submit'][] = 'commerce_extra_plus_admin_form_submit';
  
  return system_settings_form($form);
}


/**
 * Submit handler for the system settings form.
 */
function commerce_extra_plus_admin_form_submit($form, &$form_state) {
  if ($form_state['values']['commerce_extra_plus_disable_reg'] != variable_get('commerce_extra_plus_disable_reg', 1)){
      menu_rebuild();
  }
}